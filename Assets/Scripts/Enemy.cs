using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField, Tooltip("Velocit� del nemico")]
    private GameObject VFX;
    [SerializeField, Tooltip("Velocit� del nemico")]
    private float Speed;
    [SerializeField, Tooltip("Fine mappa")]
    public GameObject EndMap;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.left * Speed*Time.deltaTime;
        if (transform.position.x<EndMap.transform.position.x)
        {
            print("GameOver");
            BestScore();
            Destroy(gameObject);
            UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            GameObject Vfx = Instantiate(VFX);
            Vfx.transform.position = gameObject.transform.position;
            Destroy(gameObject);
            Destroy(Vfx, 1);
            Destroy(other.gameObject);
            int currentScore = PlayerPrefs.GetInt("Score");
            currentScore++;
            PlayerPrefs.SetInt("Score",currentScore);
            print(currentScore);
            print("BestScore: "+BestScore());

        }
        else if (other.tag == "player")
        {
            print("GameOver");
            Destroy(gameObject);
            BestScore();
            UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
        }
    }
    int BestScore()
    {
        if (PlayerPrefs.GetInt("BestScore") < PlayerPrefs.GetInt("Score"))
        {
            PlayerPrefs.SetInt("BestScore", PlayerPrefs.GetInt("Score"));
        }
        return PlayerPrefs.GetInt("BestScore");
    }
}
