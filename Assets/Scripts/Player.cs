using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Var
    [SerializeField, Tooltip("Velocit� del giocatore")]
    float speed;
    [SerializeField, Tooltip("Blocco superiore")]
    public GameObject TopBlock;
    [SerializeField, Tooltip("Blocco inferiore")]
    public GameObject BotBlock;
    [SerializeField, Tooltip("Proiettile Spawnabile")]
    public GameObject BulletPrefab;
    [SerializeField, Tooltip("Proiettile Spawnabile")]
    public GameObject multiBulletPrefab;
    bool Swap;
    #endregion

    #region callbacks
    void Start()
    {
        PlayerPrefs.SetInt("Score",0);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(Swap?multiBulletPrefab:BulletPrefab).transform.position = transform.position;
        }
        if (Input.GetKey(KeyCode.W))
        {
            Move(Vector3.up);
        }
        if (Input.GetKey(KeyCode.S))
        {
            Move(Vector3.down);
        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Swap = !Swap;
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(Swap? "Ship" : "Ship2");
        }
    }

    #endregion

    void Move(Vector3 Direction)
    {
        transform.position += Direction * speed * Time.deltaTime;
        if (transform.position.y < BotBlock.transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, BotBlock.transform.position.y, transform.position.z);
        }
        else if (transform.position.y > TopBlock.transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, TopBlock.transform.position.y, transform.position.z); 
        }
    }
}
