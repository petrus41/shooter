using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField, Tooltip("tipo di nemico")]
    private GameObject EnemyType;
    [SerializeField, Tooltip("limite inferiore")]
    private GameObject BotBlock;
    [SerializeField, Tooltip("limite superiore")]
    private GameObject TopBlock;
    [SerializeField, Tooltip("tempo massimo")]
    private float MaxTime;
    [SerializeField, Tooltip("tempo minimo")]
    private float MinTime;
    private float SpawnTimer=2;

    // Update is called once per frame
    void Update()
    {
        if (SpawnTimer>0)
        {
            SpawnTimer -= Time.deltaTime;
            if (SpawnTimer<=0)
            {
                SpawnEnemy();
                SpawnTimer = Random.Range(MinTime,MaxTime);
            }
        }
        if (Input.GetKeyDown(KeyCode.M)) 
        {
            SpawnEnemy();
        }
    }
    void SpawnEnemy()
    {
        float RandomHeight = Random.Range(BotBlock.transform.position.y,TopBlock.transform.position.y);
        Vector3 RandomPos = new Vector3(transform.position.x,RandomHeight,transform.position.z);
        Enemy NewEnemy = Instantiate(EnemyType).GetComponent<Enemy>();
        NewEnemy.transform.position = RandomPos;
        NewEnemy.EndMap = TopBlock;
    }
}
