using UnityEngine;

public class Bullet : MonoBehaviour
{
    #region var
    [SerializeField, Tooltip("Velocit� del proiettile")]
    private float speed;
    [SerializeField, Tooltip("Vita del proiettile")]
    private float LifeTime;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, LifeTime);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.right*speed*Time.deltaTime;
    }
}
