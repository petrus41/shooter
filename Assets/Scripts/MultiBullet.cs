using UnityEngine;

public class MultiBullet : MonoBehaviour
{
    public float speed;
    public bool up;
    public GameObject[] Ray;
    private void Start()
    {
        Destroy(gameObject,0.2f);
    }
    // Update is called once per frame
    void Update()
    {
        for(int i=-2;i<3;i++)
        {
            Ray[i+2].transform.Translate((Vector3.right+new Vector3(0,0.2f,0)*i)*speed*Time.deltaTime);
        }
    }
}
