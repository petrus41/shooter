using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UI_Score : MonoBehaviour
{
    [SerializeField]
    Text Score;
    [SerializeField]
    Text BestScore;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Score.text = "Score: " + PlayerPrefs.GetInt("Score").ToString();
        BestScore.text = "BestScore: " + PlayerPrefs.GetInt("BestScore").ToString();
    }
}
